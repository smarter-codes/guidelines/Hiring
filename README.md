# Hiring

At Smarter.Codes we embrace this value of [Colleague > Customer](https://wiki.smarter.codes/values/#Values-1.Colleague%3ECustomer).
The right colleague can bring in 1000s of happy customers. In this project we would discuss practices that help ensures that
Smarter.Codes is joined by the best set of Engineers, Marketers, Sales, HR or Product Designers.

Depending upon your department please click a link below. If your department does not exists below already please draw inspiration from the links below and create a new project for your department

* for [Software Engineering](https://gitlab.com/smarter-codes/guidelines/software-engineering/hiring)
* for Marketers
* for Sales
* for Product Design
* for HR

# Notable Guidelines

* [Communicate as if we are in Candidate Market. Than Employer market](https://gitlab.com/smarter-codes/guidelines/Hiring/-/issues/2)

# Inspirations

https://rework.withgoogle.com/subjects/hiring/
